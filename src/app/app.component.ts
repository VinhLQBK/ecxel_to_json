import {Component, OnInit} from '@angular/core';
import {LanguageConfig} from '../config/language-config';
// import {ExcelJsonService} from './excel-json.service';
// import * as excelToJson from 'excel-as-json';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  languages: string[]; // danh sach cac ngon ngu co the translate
  language: string; // ngôn ngữ hiện tại

  constructor(public langConfig: LanguageConfig) {
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.languages = this.langConfig.getAllLanguage();
    this.langConfig.setLanguageUser();
    this.language = this.langConfig.getCookieLang('lang');
  }

  /* thiết lập ngôn ngữ muốn translate */
  setLanguage(language: string) {
    this.langConfig.useLanguage(language);
    this.langConfig.setCookieLang('lang', language, 30);
  }

}
