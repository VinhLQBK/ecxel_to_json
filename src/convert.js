var fs = require('fs'), path = require('path');
var XLSX = require('xlsx');

(() => {
  const fileName = path.join(__dirname, '/assets/excel/LPICLabelMessages.xlsx');
  var wb = XLSX.readFile(fileName);
  data = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[0]], {header: 1});
  convertJson(data, 'en.json', 3, 1, 2);
  convertJson(data, 'ja.json', 3, 1, 3);
  convertJson(data, 'vn.json', 3, 1, 4);

})();

function convertJson(data, fileName, rowStart, key, colValue) {
  const fileNameJson = path.join(__dirname, '/assets/i18n/' + fileName);
  objectJson = {};
  console.log(fileNameJson);
  for (let i = rowStart; i < data.length; i++) {
    if (data[i][key] != null && data[i][colValue] != null) {
      objectJson[data[i][key].toString().trim()] = data[i][colValue].toString().trim();
    }
  }
  fs.writeFile(
    fileNameJson,

    JSON.stringify(objectJson),

    function (err) {
      if (err) {
        console.error('Crap happens');
      }
    }
  );
}



