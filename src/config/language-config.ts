import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class LanguageConfig {
  constructor(public translate: TranslateService) {
    this.init();
  }

  /* set mang ngon ngu va set ngon ngư mac dinh */
  init() {
    this.translate.addLangs(['en', 'ja', 'vn']);
    this.setDefaultlang();
  }

  /*
  - Nếu ngôn ngữ chưa được lưu trong cookie thì set mặc định là en
  - Nếu có thì lấy theo ngôn ngữ trong cookie
  * */
  setDefaultlang() {
    const langCookie = this.getCookieLang('lang');
    if (langCookie == null) {
      this.translate.setDefaultLang('en');
    } else {
      this.translate.setDefaultLang(langCookie);
    }
  }

  addlanguage(language: string[]) {
    this.translate.addLangs(language);
  }

  /*Lựa chọn ngôn ngữ muốn translate*/
  useLanguage(language: string) {
    this.translate.use(language);
  }

  /* thiết lập ngôn ngữ trong cookie*/
  setLanguageUser() {
    const uselang: string = this.getCookieLang('lang');
    this.useLanguage(uselang);
  }

  /* lấy ngôn ngữ lưu trong cookie */
  getCookieLang(cname): string {
    const name = cname + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  }

  /* lưu ngôn ngữ vào trong cookie*/
  setCookieLang(cname, cvalue, exdays): void {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
  }

  /* lấy tất cả các ngôn ngữ trong mảng ngôn ngữ translate */
  getAllLanguage(): string[] {
    return this.translate.getLangs();
  }

}
